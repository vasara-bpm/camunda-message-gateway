# -*- coding: utf-8 -*-
"""Test token and decoding."""
from fastapi.security.http import HTTPAuthorizationCredentials
from operaton_message_gateway.config import settings
from operaton_message_gateway.main import decode_token
from operaton_message_gateway.main import gen_token
from starlette.exceptions import HTTPException
import operaton_message_gateway.types.api as t_api
import pytest
import time


settings.TOKEN_SECRET_KEY = """\
-----BEGIN PRIVATE KEY-----
MC4CAQAwBQYDK2VwBCIEIJqXNNXBxOzkOSQ4LQ9L3MUG1CdM5HXxz+y1DKEm2q9n
-----END PRIVATE KEY-----
"""

settings.TOKEN_PUBLIC_KEY = """\
-----BEGIN PUBLIC KEY-----
MCowBQYDK2VwAyEAtjCPy/HJaJSZDJ1BvDWajILwJWEyBuy0L/rM1938FCs=
-----END PUBLIC KEY-----
"""

ALLOWED_MESSAGE_NAME = "OperatonMessageGateway"


def test_invalid_authorization_scheme() -> None:
    content = t_api.MessagingToken(name=ALLOWED_MESSAGE_NAME)
    token = gen_token(content)
    with pytest.raises(HTTPException):
        credentials = HTTPAuthorizationCredentials(scheme="Basic", credentials=token)
        content = decode_token(credentials)


def test_invalid_message_name() -> None:
    content = t_api.MessagingToken(name="")
    token = gen_token(content)
    with pytest.raises(HTTPException):
        credentials = HTTPAuthorizationCredentials(scheme="Basic", credentials=token)
        content = decode_token(credentials)


def test_token_encode_decode() -> None:
    content = t_api.MessagingToken(name=ALLOWED_MESSAGE_NAME)
    token = gen_token(content, days=1)
    credentials = HTTPAuthorizationCredentials(scheme="Bearer", credentials=token)
    content = decode_token(credentials)
    assert content.name == ALLOWED_MESSAGE_NAME


def test_token_valid() -> None:
    content = t_api.MessagingToken(name=ALLOWED_MESSAGE_NAME)
    token = gen_token(content, days=1)
    credentials = HTTPAuthorizationCredentials(scheme="Bearer", credentials=token)
    content = decode_token(credentials)
    assert content.name == ALLOWED_MESSAGE_NAME


def test_token_expired() -> None:
    content = t_api.MessagingToken(name=ALLOWED_MESSAGE_NAME)
    token = gen_token(content, days=0)
    credentials = HTTPAuthorizationCredentials(scheme="Bearer", credentials=token)
    time.sleep(1)
    with pytest.raises(HTTPException):
        content = decode_token(credentials)
