from operaton_message_gateway.main import gen_token
from operaton_message_gateway.types import api as t_api
from robot.api.deco import library
from robot.libraries import BuiltIn  # type: ignore
from typing import Any


@library(scope="GLOBAL", listener="SELF")
class Authorization:
    def start_suite(self, *args: Any, **kwargs: Any) -> None:
        token = gen_token(t_api.MessagingToken(name="OperatonMessageGateway"), days=1)
        BuiltIn.BuiltIn().set_global_variable("${AUTHORIZATION}", f"Bearer {token}")
