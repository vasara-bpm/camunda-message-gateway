*** Settings ***
Library     Collections
Library     requests
Library     Authorization.py


*** Variables ***
${GATEWAY_URL}      %{GATEWAY_URL=http://localhost:8000/api/v1}
${AUTHORIZATION}    Bearer %{TOKEN_GATEWAY=n/a}


*** Test Cases ***
Start process and message with passed id
    ${headers}=    Get headers
    ${processVariables}=    Create process variables    correlationId=Hello World!
    ${payload}=    Create Dictionary    processVariables=${processVariables}
    ${response}=    Post    ${GATEWAY_URL}/message    headers=${headers}    json=${payload}

    # A start event should have been correlated and a new process created
    Should Be True    ${response.ok}    ${response.json()}
    Dictionary Should Contain key    ${response.json()}    businessKey
    Should Be Equal    "${response.status_code}"    "201"

    # Sleep    1 second
    Sleep    1 second

    ${businessKey}=    Get From Dictionary    ${response.json()}    businessKey
    ${payload}=    Create dictionary    businessKey=${businessKey}    messageCorrelationId=Hello World!
    ${response}=    Post    ${GATEWAY_URL}/message    headers=${headers}    json=${payload}

    # An intermediate catch event or task should have been correlated
    Should Be True    ${response.ok}    ${response.text}
    Should Be Equal    "${response.status_code}"    "204"

    # Sleep    1 second
    Sleep    1 second

    ${payload}=    Create Dictionary    processVariables=${processVariables}
    ${response}=    Post    ${GATEWAY_URL}/message    headers=${headers}    json=${payload}

    # As the previous process should have been compled, the same payload starts a new process
    Should Be True    ${response.ok}    ${response.json()}
    Dictionary Should Contain key    ${response.json()}    businessKey
    Should Be Equal    "${response.status_code}"    "201"

    # And now we clean up

    # Sleep    1 second
    Sleep    1 second

    ${businessKey}=    Get From Dictionary    ${response.json()}    businessKey
    ${payload}=    Create dictionary    businessKey=${businessKey}    messageCorrelationId=Hello World!
    ${response}=    Post    ${GATEWAY_URL}/message    headers=${headers}    json=${payload}

    # An intermediate catch event or task should have been correlated
    Should Be True    ${response.ok}    ${response.text}
    Should Be Equal    "${response.status_code}"    "204"

    # Sleep    1 second
    Sleep    1 second

Start process and query variables
    ${headers}=    Get headers
    ${processVariables}=    Create process variables    messageCorrelationId=Hello World!
    ${payload}=    Create Dictionary    messageName=start    processVariables=${processVariables}
    ${response}=    Post    ${GATEWAY_URL}/message    headers=${headers}    json=${payload}
    ${businessKey}=    Get From Dictionary    ${response.json()}    businessKey

    # A start event should have been correlated and a new process created
    Should Be True    ${response.ok}    ${response.json()}
    Dictionary Should Contain key    ${response.json()}    businessKey
    Should Be Equal    "${response.status_code}"    "201"

    ${payload}=    Create Dictionary
    ...    messageName=status
    ...    businessKey=${businessKey}
    ...    messageCorrelationId=Hello World!
    ...    variablesInResultEnabled=true
    ${response}=    Post    ${GATEWAY_URL}/message    headers=${headers}    json=${payload}

    # An intermediate catch event or task should have been correlated
    Should Be True    ${response.ok}    ${response.text}
    Should Be Equal    "${response.status_code}"    "200"
    Log    ${response.json()}
    Dictionary Should Contain key    ${response.json()}    variables

    # And the response should contain the variables we expect
    ${variables}=    Get From Dictionary    ${response.json()}    variables
    Dictionary Should Contain key    ${variables}    visitorToken
    Dictionary Should Contain key    ${variables}    messageCorrelationId

    # Sleep    1 second
    Sleep    1 second

    # And now we clean up
    ${payload}=    Create Dictionary
    ...    messageName=complete
    ...    businessKey=${businessKey}
    ${response}=    Post    ${GATEWAY_URL}/message    headers=${headers}    json=${payload}

    # An intermediate catch event or task should have been correlated
    Should Be True    ${response.ok}    ${response.text}
    Should Be Equal    "${response.status_code}"    "204"


*** Keywords ***
Get headers
    ${headers}=    Create Dictionary
    ...    Authorization=${AUTHORIZATION}
    ...    Content-Type=application/json
    ...    Accept=application/jsonj
    RETURN    ${headers}

Create Camunda variable
    [Arguments]    ${type}    ${value}
    ${variable}=    Create Dictionary
    ...    type=${type}    value=${value}
    RETURN    ${variable}

Create process variables
    [Arguments]    &{variables}
    ${processVariables}=    Create Dictionary
    ${keys}=    Get Dictionary Keys    ${variables}
    FOR    ${key}    IN    @{keys}
        ${value}=    Get From Dictionary    ${variables}    ${key}
        ${variable}=    Create Camunda variable    String    ${value}
        Set To Dictionary    ${processVariables}    ${key}=${variable}
    END
    RETURN    ${processVariables}
