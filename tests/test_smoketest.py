# -*- coding: utf-8 -*-
"""Test that project is importable."""


def test_import() -> None:
    import operaton_message_gateway.main  # noqa
