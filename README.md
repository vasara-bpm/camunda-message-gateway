# operaton-message-gateway

Opinionated minimal OpenAPI service as a guarded proxy for Operaton REST API /message endpoint


## Use case

External application needs to be able to interact with operaton, but only in a very limited way, which should be controlled already when modeling the diagrams:

* need to start a process
* pass subsequent messages to process when allowed in the process
* need to query some variables from the process

In addition, the business key for the created process should be returned. Yet, message correlation for started processes is only done with the correlation id passed in the start.


## The example setup

The example Operaton deployment contains two interesting related processes:

* Message Gateway: Dispatch a new process
* Message Gateway: Example process
* Message Gateway: Poll process state

Both of the processes listen the same fixed message `OperatonMessageGateway`.


## Usage

0. Generate token with an allowed message name (or message name prefix):

   ```
   python -c "from operaton_message_gateway.main import gen_token; import operaton_message_gateway.types.api as t_api; print(gen_token(t_api.MessagingToken(name='OperatonMessageGateway', days=365)))"
   ```

1. Message without or without business key to correlate a start event:

   ```
   $ curl -X 'POST' \
     'http://localhost:8000/api/v1/message' \
     -H 'accept: application/json' \
     -H "Authorization: Bearer $TOKEN" \
     -H 'Content-Type: application/json' \
     -d '{
     "processVariables": {"correlationId": {"type": "String", "value": "Hello World!"}}
   }'
   ```

   API calls Operaton with the given or generated business key:
   ```
   {"messageName": "OperatonMessageGateway", "businessKey": "c04659fe-8c7a-4adb-964a-55ffa11c2f93", "localCorrelationKeys": {}, "processVariablesLocal": {"correlationId": {"value": "Hello World!", "type": "String"}}, "resultEnabled": true, "variablesInResultEnabled": false, "all": false}
   ```

   (In the example, passed local variable `correlationId` is mapped as a local variable `messageCorrelationId` of an event listener task.)

   Operaton responds with:

   ```
   {"resultType": "ProcessDefinition", "execution": null, "processInstance": {"links": [], "id": "ff06b15d-972b-11ed-a231-fe3a965c18c8", "definitionId": "5f585106-9729-11ed-a231-fe3a965c18c8", "businessKey": "c04659fe-8c7a-4adb-964a-55ffa11c2f93", "caseInstanceId": null, "ended": false, "suspended": false, "tenantId": null}, "variables": {"messageName": {"type": "String", "value": "OperatonMessageGatewayExample", "valueInfo": {}}, "correlationId": {"type": "String", "value": "Hello World!", "valueInfo": {}}}}
   ```

   API responds with 201 and the given or generated business key:

   ```
   {"businessKey":"c04659fe-8c7a-4adb-964a-55ffa11c2f93"}
   ```

2. Communicate with the process started previously:

   ```
   $ curl -X 'POST' \
     'http://localhost:8000/api/v1/message' \
     -H 'accept: application/json' \
     -H "Authorization: Bearer $TOKEN" \
     -H 'Content-Type: application/json' \
     -d '{
     "businessKey":"c04659fe-8c7a-4adb-964a-55ffa11c2f93"
     "messageCorrelationId": "Hello World!",
   }'
   ```

   API calls Operaton with:

   ```
   {"messageName": "nperatonMessageGateway", "businessKey": "c04659fe-8c7a-4adb-964a-55ffa11c2f93", "localCorrelationKeys": {"messageCorrelationId": {"value": "Hello World!", "type": "String"}}, "processVariablesLocal": {}, "resultEnabled": true, "variablesInResultEnabled": false, "all": false}
   ```

   Operaton responds with:

   ```
   {"resultType": "Execution", "execution": {"id": "ff0774bd-972b-11ed-a231-fe3a965c18c8", "processInstanceId": "ff074da7-972b-11ed-a231-fe3a965c18c8", "ended": true, "tenantId": null}, "processInstance": null, "variables": {"messageName": {"type": "String", "value": "OperatonMessageGatewayExample", "valueInfo": {}}, "correlationId": {"type": "String", "value": "Hello World!", "valueInfo": {}}}}
   ```

   API responds with 204.

3. Alternatively, it is possibly to use prefixed message name with `messageName`:

   ```
   $ curl -X 'POST' \
    'http://localhost:8000/api/v1/message' \
    -H 'accept: application/json' \
    -H "Authorization: Bearer $TOKEN" \
    -H 'Content-Type: application/json' \
    -d '{
    "messageName": "start",
    "processVariables": {"messageCorrelationId": {"type": "String", "value": "Hello World!"}}
   }'
   ```

   For subsequent calls with prefixed message names other than `start`, it is possible
   to use `messageCorrelationId` instead of `businessKey`.

   ```
   $ curl -X 'POST' \
    'http://localhost:8000/api/v1/message' \
    -H 'accept: application/json' \
    -H "Authorization: Bearer $TOKEN" \
    -H 'Content-Type: application/json' \
    -d '{
    "messageName": "status",
    "messageCorrelationId": "Hello World!"
   }'
   ```

   Allowed variables could be returned with `variablesInResultEnabled=true`.


## Configuration

Create private key for signing access tokens:

```
$ openssl genpkey -algorithm ed25519 -out ec-ed25519-priv-key.pem
```

Create public key for validating access tokens:

```
$ openssl ec -in ec-ed25519-priv-key.pem -pubout > ec-ed25519-pub-key.pem
```

Configure environment variables:

```
# Read JWT signing public key
public_key=$(cat ec-ed25519-pub-key.pem);
export TOKEN_PUBLIC_KEY=$public_key

# Read JWT signing public key
private_key=$(cat ec-ed25519-priv-key.pem);
export TOKEN_SECRET_KEY=$private_key

export MESSAGE_NAME_ALLOW=OperatonMessageGateway
export VARIABLES_ALLOW=messageCorrelationId,visitorToken

# Set Operaton API and authorization
export ENGINE_REST_BASE_URL=http://localhost:8080/engine-rest
export ENGINE_REST_AUTHORIZATION=Basic ZGVtbzpkZW1v
```
