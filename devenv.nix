{
  pkgs,
  lib,
  config,
  inputs,
  ...
}:
{
  imports = [
    ./devenv/modules/image.nix
    ./devenv/modules/operaton.nix
    ./devenv/modules/python.nix
  ];

  package.image.group = "vasara-bpm";
  package.image.repo = "camunda-message-gateway";
  package.image.name = "operaton-message-gateway";
  package.image.package = config.outputs.python.app;

  package.operaton.path = ./fixture;

  languages.python.interpreter = pkgs.python312;
  languages.python.pyprojectOverrides = final: prev: {
    "operaton-tasks" = prev."operaton-tasks".overrideAttrs (old: {
      nativeBuildInputs =
        old.nativeBuildInputs
        ++ final.resolveBuildSystem ({
          "hatchling" = [ ];
        });
    });
  };

  packages = [
    pkgs.entr
    pkgs.findutils
    pkgs.gnumake
    pkgs.openssl
  ];

  dotenv.disableHint = true;

  enterShell = ''
    unset PYTHONPATH
    export UV_NO_SYNC=1
    export UV_PYTHON_DOWNLOADS=never
    export REPO_ROOT=$(git rev-parse --show-toplevel)
  '';

  processes.gateway.exec = "make -s watch";

  enterTest = ''
    wait_for_port 8000 60
    wait_for_port 8080 60
    make -s test test-robot
  '';

  cachix.pull = [ "vasara-bpm" ];

  devcontainer.enable = true;

  git-hooks.hooks.treefmt = {
    enable = true;
    settings.formatters = [
      pkgs.nixfmt-rfc-style
    ];
  };
}
