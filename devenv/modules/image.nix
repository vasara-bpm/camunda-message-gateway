{
  pkgs,
  config,
  lib,
  ...
}:
let
  cfg = config.package.image;
  inherit (lib) types mkOption;
in
{
  options.package.image = {
    group = mkOption {
      type = types.str;
    };
    repo = mkOption {
      type = types.str;
    };
    name = mkOption {
      type = types.str;
    };
    package = mkOption {
      type = config.lib.types.outputOf types.package;
    };
    callable = mkOption {
      type = types.str;
      default = config.package.image.name;
    };
  };
  config.outputs.image = pkgs.dockerTools.streamLayeredImage {
    name = "${cfg.group}/${cfg.repo}/${cfg.name}";
    tag = "latest";
    created = "now";
    maxLayers = 2;
    contents = [
      (pkgs.dockerTools.fakeNss.override {
        extraPasswdLines = [
          "container:x:65:65:Container user:/var/empty:/bin/sh"
        ];
        extraGroupLines = [
          "container:!:65:"
        ];
      })
      pkgs.busybox
      pkgs.dockerTools.usrBinEnv
      pkgs.tini
      cfg.package
    ];
    extraCommands = ''
      mkdir -p tmp && chmod a+rxwt tmp
    '';
    config = {
      Entrypoint = [
        "${pkgs.tini}/bin/tini"
        "--"
        "${cfg.package}/bin/${cfg.callable}"
      ];
      Env = [
        "TMPDIR=/tmp"
        "HOME=/tmp"
      ];
      Labels = builtins.fromJSON (builtins.readFile ../../Labels.json);
      User = "nobody";
    };
  };
}
