#!/usr/bin/env bash
source .envsh
echo "Token for messages prefixed with $1 for $2 days"
python3.12 -c "from operaton_message_gateway.main import gen_token; import operaton_message_gateway.types.api as t_api; print(gen_token(t_api.MessagingToken(name='$1'), days=$2))"
