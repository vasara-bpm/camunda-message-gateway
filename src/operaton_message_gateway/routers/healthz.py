"""FastAPI router."""

from datetime import datetime
from fastapi.routing import APIRouter
from operaton_message_gateway.types.heartbeat import Heartbeat


router = APIRouter()


@router.get(
    "/healthz", response_model=Heartbeat, summary="Service health status", tags=["Meta"]
)
async def healthz() -> Heartbeat:
    """Service health status."""
    now = datetime.utcnow()
    return Heartbeat(timestamp=now.isoformat())
