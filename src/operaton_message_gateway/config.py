"""Settings and logging for the application."""

from operaton.tasks.config import settings as task_settings  # type: ignore
from pathlib import Path
from pydantic import BaseModel
from pydantic import FilePath
from pydantic import HttpUrl
from pydantic import model_validator
from pydantic_settings import BaseSettings
from pydantic_settings import SettingsConfigDict
from typing import Any
from typing import Dict
from typing import List
from typing_extensions import Self
import json
import logging
import os


class MessageGatewayTaskProperty(BaseModel):
    """Gateway message task property."""

    description: str


class MessageGatewayTaskProperties(BaseModel):
    """Gateway message task properties."""

    messageName: MessageGatewayTaskProperty


class MessageGatewayTaskSchema(BaseModel):
    """Gateway message task schema."""

    properties: MessageGatewayTaskProperties


class MessageGatewayToken(BaseModel):
    """Token identifying the sender of a request."""

    url: HttpUrl
    token: str


class MessageGatewayTask(BaseModel):
    """Gateway message task."""

    url: HttpUrl
    token: str
    json_schema: Any


class Settings(BaseSettings):
    """App configuration settings."""

    model_config = SettingsConfigDict(
        env_file=".env",
        env_file_encoding="utf-8",
        secrets_dir=os.environ.get("SECRETS_DIR"),
    )

    AIOHTTP_TIMEOUT: int = 10
    LOG_LEVEL: str = "INFO"

    ENGINE_REST_BASE_URL: str = "http://localhost:8080/engine-rest"
    ENGINE_REST_AUTHORIZATION: str = ""

    MESSAGE_SCOPE_RESOLUTION: str = ":"
    MESSAGE_CORRELATION_NAME: str = "messageCorrelationId"
    MESSAGE_NAME_ALLOW: str = "OperatonMessageGateway"  # Comma separated str
    MESSAGE_NAME_ALLOW_LIST: List[str] = []

    VARIABLES_ALLOW: str = ""
    VARIABLES_ALLOW_LIST: List[str] = []

    TOKEN_SECRET_KEY: str = ""
    TOKEN_PUBLIC_KEY: str = ""

    TASKS_TOPIC: str = "message-gateway"
    TASKS_WORKER_ID: str = "operaton-message-gateway"
    TASKS_GATEWAYS: Dict[str, MessageGatewayToken] = {}
    TASKS_DEFINITIONS: List[FilePath] = []
    TASKS: Dict[str, MessageGatewayTask] = {}

    @model_validator(mode="after")
    def interpolate(self: Self) -> Self:
        """Calculate expires_in from exp if exp is present."""
        # Parse allowed message prefixes in tokens
        if self.MESSAGE_NAME_ALLOW:
            self.MESSAGE_NAME_ALLOW_LIST = [
                s.strip() for s in self.MESSAGE_NAME_ALLOW.split(",")
            ]
        # Parse allowed variables in responses
        if self.VARIABLES_ALLOW:
            self.VARIABLES_ALLOW_LIST = [
                s.strip() for s in self.VARIABLES_ALLOW.split(",")
            ]
        # Parse messaging tasks
        for path in self.TASKS_DEFINITIONS:
            schema = json.loads(Path(path).read_text())
            name = MessageGatewayTaskSchema(**schema).properties.messageName.description
            for key, value in sorted(self.TASKS_GATEWAYS.items(), key=lambda x: x[0]):
                if name.startswith(key):
                    self.TASKS[name] = MessageGatewayTask(
                        url=value.url,
                        token=value.token,
                        json_schema=json.dumps(schema),
                    )
            assert name in self.TASKS, f"Gateway configuration missing for task {name}"
        return self


settings = Settings()

task_settings.ENGINE_REST_BASE_URL = settings.ENGINE_REST_BASE_URL
task_settings.TASKS_WORKER_ID = settings.TASKS_WORKER_ID

formatter = logging.Formatter(
    "%(asctime)s | %(levelname)s | %(name)s:%(lineno)d | %(message)s",
    "%d-%m-%Y %H:%M:%S",
)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(settings.LOG_LEVEL)

logger = logging.getLogger("operaton_message_gateway")
logger.addHandler(stream_handler)
logger.setLevel(settings.LOG_LEVEL)
logger.propagate = False

__all__ = ["logger", "settings"]
