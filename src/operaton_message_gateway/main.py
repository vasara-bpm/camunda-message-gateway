"""Microservice to proxy requests to Operaton with access control and a simplified API."""

from contextlib import asynccontextmanager
from datetime import datetime
from datetime import timedelta
from datetime import timezone
from fastapi import status
from fastapi.applications import FastAPI
from fastapi.params import Depends
from fastapi.security.http import HTTPAuthorizationCredentials
from fastapi.security.http import HTTPBearer
from operaton.tasks.config import handlers  # type: ignore
from operaton.tasks.worker import external_task_worker  # type: ignore
from operaton_message_gateway import worker
from operaton_message_gateway.routers.healthz import router as healthz_router
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import Response
from typing import Any
from typing import AsyncGenerator
from typing import Awaitable
from typing import Callable
from typing import Dict
from typing import Optional
import aiohttp
import asyncio
import json
import jwt
import logging
import operaton_message_gateway.config as cfg
import operaton_message_gateway.types.api as api_t
import operaton_message_gateway.types.engine as engine_t
import sys
import uuid
import uvicorn


assert worker


logger = logging.getLogger(__name__)


@asynccontextmanager
async def lifespan(app: FastAPI) -> AsyncGenerator[Any, Any]:
    """Start external task worker on FastAPI startup."""
    if cfg.settings.TASKS:
        asyncio.ensure_future(external_task_worker(handlers))
        logger.info("Event loop: %s", asyncio.get_event_loop())
    yield


app = FastAPI(lifespan=lifespan)
app.include_router(healthz_router)


class LazyJsonDump:
    """Help to serialize object only when really interpolated."""

    def __init__(self, value: Any):
        """Initialize."""
        self.value = value

    def __str__(self) -> str:
        """Serialize."""
        return json.dumps(self.value)


@app.middleware("http")
async def cache_headers(
    request: Request, call_next: Callable[[Request], Awaitable[Response]]
) -> Response:
    """Set cache headers."""
    response = await call_next(request)
    response.headers["Cache-Control"] = "no-store, max-age=0"
    return response


def decode_token(
    token: HTTPAuthorizationCredentials = Depends(HTTPBearer()),  # type: ignore
) -> api_t.MessagingToken:
    """Decode request sender info from the Authorization header."""

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
    )
    if token is None:
        raise credentials_exception

    if token.scheme.lower() != "bearer":
        raise credentials_exception

    try:
        content = api_t.MessagingToken(
            **jwt.decode(
                token.credentials,
                cfg.settings.TOKEN_PUBLIC_KEY,
                algorithms=["ES256", "EdDSA"],
            )
        )
    except Exception as ex:
        credentials_exception.detail = f"Could not validate credentials: {ex}"
        raise credentials_exception from ex

    if content.name not in cfg.settings.MESSAGE_NAME_ALLOW_LIST:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f"Token's message prefix '{content.name}' is missing from the allow list",
        )

    return content


def error(status_code: int, err_body: Dict[str, Any]) -> HTTPException:
    """Figure out why a Operaton error occurred and return a more descriptive error to the user."""

    err_type = err_body.get("type")
    err_message = err_body.get("message")
    if err_type and err_message:
        if (
            status_code == 400
            and err_type == "RestException"
            and err_message.find("MismatchingMessageCorrelationException") >= 0
        ):
            return HTTPException(
                status_code=500,
                detail=api_t.MessageError(
                    message="None or multiple processes for given correlation",
                    originalError=err_body,
                ).__dict__,  # type: ignore
                # (mypy thinks this can only be str, but anything jsonable actually works)
            )

    return HTTPException(
        status_code=status_code,
        detail=api_t.MessageError(
            message="Engine internal error",
            originalError=err_body,
        ).__dict__,  # type: ignore
    )


def build_message_name(params: api_t.MessageParams, token: api_t.MessagingToken) -> str:
    """Remove token message name from message name when prefixed."""
    scope_resolution = cfg.settings.MESSAGE_SCOPE_RESOLUTION
    prefix = f"{token.name}{scope_resolution}"
    if not params.messageName:
        return token.name
    elif params.messageName.startswith(prefix):
        return params.messageName.strip(scope_resolution)
    else:
        return f"{prefix}{params.messageName}".strip(scope_resolution)


def serialize_json_variables(
    variables: Optional[Dict[str, engine_t.TypedValue]]
) -> Optional[Dict[str, engine_t.TypedValue]]:
    """Serialize JSON variable values values."""
    if not variables:
        return variables
    for key in variables:
        if variables[key].type == "Json":
            if not isinstance(variables[key].value, str):
                variables[key].value = json.dumps(variables[key].value)
    return variables


@app.get("/api/v1/token", summary="Validate token", tags=["Meta"])
async def get_token(
    token: api_t.MessagingToken = Depends(decode_token),  # type: ignore # pylint: disable=W0613
) -> api_t.MessagingToken:
    """Validate and return request token."""
    return token


@app.post("/api/v1/message", summary="Correlate message", tags=["Message"])
async def post_message(
    params: api_t.MessageParams,
    resp: Response,
    token: api_t.MessagingToken = Depends(decode_token),  # type: ignore # pylint: disable=W0613
) -> api_t.MessageResponse:
    """Send a message to Operaton."""

    # Serialize unserialized JSON values in variables
    params.processVariables = serialize_json_variables(params.processVariables)

    async with aiohttp.ClientSession(
        headers=(
            {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": cfg.settings.ENGINE_REST_AUTHORIZATION,
            }
            if cfg.settings.ENGINE_REST_AUTHORIZATION
            else {"Content-Type": "application/json", "Accept": "application/json"}
        ),
        timeout=aiohttp.ClientTimeout(cfg.settings.AIOHTTP_TIMEOUT),
    ) as http:

        # Build message name
        message_name = build_message_name(params, token)

        # Ensure business key when message name implies start event
        if not params.businessKey and (
            message_name == token.name
            or message_name.endswith(f"{cfg.settings.MESSAGE_SCOPE_RESOLUTION}start")
            or message_name.endswith(f"{cfg.settings.MESSAGE_SCOPE_RESOLUTION}aloita")
            # Always support dot, because it is so popular
            or message_name.endswith(".start")
            or message_name.endswith(".aloita")
        ):
            params.businessKey = str(uuid.uuid4())

        # Correlate message
        payload = engine_t.MessageToProcess(
            messageName=message_name,
            businessKey=params.businessKey,
            localCorrelationKeys=engine_t.CorrelationKeyMap(
                messageCorrelationId=(
                    engine_t.TypedValue(
                        value=params.messageCorrelationId,
                        type="String",
                    )
                    if params.messageCorrelationId
                    else None
                ),
            ),
            processVariablesLocal=params.processVariables or {},
            variablesInResultEnabled=params.variablesInResultEnabled,
        )
        payload_json = json.dumps(payload.to_dict())
        logger.warn("%s", payload_json)
        for retry in range(1, 3):
            async with http.post(
                f"{cfg.settings.ENGINE_REST_BASE_URL}/message",
                data=payload_json,
            ) as correlation_resp:
                if correlation_resp.status != 200:
                    msg = await correlation_resp.text()
                    if "OptimisticLockingException" in msg:
                        await asyncio.sleep(retry)
                        continue
                    raise error(correlation_resp.status, await correlation_resp.json())
                for correlation_result in await correlation_resp.json() or []:
                    logger.debug("%s", LazyJsonDump(correlation_result))
                    variables = correlation_result.get("variables") or {}

                    # Message correlated to start event
                    if correlation_result.get("resultType") != "Execution":
                        resp.status_code = 201
                        assert (
                            params.businessKey
                        ), "Message correlated with start event without businessKey."
                        return (
                            api_t.NewProcessResponseWithVariables(
                                businessKey=params.businessKey,
                                variables=(
                                    {
                                        k: v
                                        for k, v in variables.items()
                                        if k in cfg.settings.VARIABLES_ALLOW_LIST
                                    }
                                ),
                            )
                            if params.variablesInResultEnabled
                            else api_t.NewProcessResponse(
                                businessKey=params.businessKey,
                            )
                        )

                    # Message correlated to intermediate event
                    if not params.variablesInResultEnabled:
                        resp.status_code = 204
                        return api_t.CorrelatedProcessResponse()

                    variables = correlation_result.get("variables") or {}
                    return api_t.CorrelatedProcessResponseWithVariables(
                        variables=(
                            (
                                {
                                    k: v
                                    for k, v in variables.items()
                                    if k in cfg.settings.VARIABLES_ALLOW_LIST
                                }
                            )
                            if params.variablesInResultEnabled
                            else None
                        ),
                    )

        raise HTTPException(
            status_code=400,
            detail=api_t.MessageError(
                message="Request did not not correlate"
            ).__dict__,  # type: ignore
            # (mypy thinks this can only be str, but anything jsonable actually works)
        )


def gen_token(
    content: api_t.MessagingToken,
    expires: Optional[datetime] = None,
    days: Optional[int] = None,
) -> str:
    """Generate an authorizing token for a service."""
    exp = (
        datetime.now(tz=timezone.utc) + timedelta(days=days)
        if days is not None
        else expires if datetime is not None else None
    )
    return jwt.encode(
        {key: value for key, value in content.__dict__.items() if value is not None}
        | ({"exp": exp} if exp is not None else {}),
        cfg.settings.TOKEN_SECRET_KEY,
        algorithm="EdDSA",
    )


def main() -> None:
    """Run Operaton Message Gateway."""
    sys.argv.insert(1, "operaton_message_gateway.main:app")
    sys.argv.insert(2, "--proxy-headers")
    uvicorn.main()
