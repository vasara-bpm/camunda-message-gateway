from aiohttp import ClientTimeout
from contextlib import asynccontextmanager
from lxml.etree import Element
from lxml.etree import fromstring
from operaton.tasks import task  # type: ignore
from operaton.tasks.types import CompleteExternalTaskDto  # type: ignore
from operaton.tasks.types import ExternalTaskComplete
from operaton.tasks.types import LockedExternalTaskDto
from operaton.tasks.types import ProcessDefinitionDiagramDto
from operaton.tasks.types import VariableValueDto
from operaton.tasks.utils import verify_response_status  # type: ignore
from operaton_message_gateway.config import settings
from operaton_message_gateway.types.api import CorrelatedProcessResponseWithVariables
from operaton_message_gateway.types.api import MessageParams
from operaton_message_gateway.types.api import NewProcessResponseWithVariables
from operaton_message_gateway.types.engine import TypedValue
from pydantic import HttpUrl
from typing import Any
from typing import AsyncGenerator
from typing import Dict
from typing import Optional
from typing import Union
import aiocache  # type: ignore
import aiohttp
import json
import jsonschema
import re


NS = {"bpmn": "http://www.omg.org/spec/BPMN/20100524/MODEL"}

RE_ENGINE_DATE = re.compile(r"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}\+\d{4}$")


@asynccontextmanager
async def operaton_session(
    authorization: str = settings.ENGINE_REST_AUTHORIZATION,
) -> AsyncGenerator[aiohttp.ClientSession, None]:
    """Get aiohttp session with Operaton headers."""
    headers = (
        {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": authorization,
        }
        if authorization
        else {"Content-Type": "application/json", "Accept": "application/json"}
    )
    async with aiohttp.ClientSession(
        headers=headers,
        trust_env=True,
        timeout=ClientTimeout(total=settings.AIOHTTP_TIMEOUT),
    ) as session:
        yield session


@aiocache.cached(ttl=3600 * 24)  # type: ignore
async def get_xml(processDefinitionId: str) -> Element:  # type: ignore
    """Get the BPMN XML for a process definition."""
    async with operaton_session() as session:
        url = f"{settings.ENGINE_REST_BASE_URL}/process-definition/{processDefinitionId}/xml"
        get = await session.get(url)
        await verify_response_status(get, status=(200,))
        return fromstring(
            ProcessDefinitionDiagramDto(**await get.json()).bpmn20Xml.encode("utf-8")
        )


def load_variables(variables: Dict[str, VariableValueDto]) -> Dict[str, Any]:
    """Load variables from a dictionary of VariableValueDto."""
    return {
        key: (
            json.loads(variable.value)
            if (variable.valueInfo or {}).get("serializationDataFormat")
            == "application/json"
            or variable.type == "Json"
            else variable.value
        )
        for key, variable in variables.items()
    }


async def correlate(
    gateway: HttpUrl, token: str, payload: MessageParams
) -> Union[NewProcessResponseWithVariables, CorrelatedProcessResponseWithVariables]:
    async with aiohttp.ClientSession(
        headers={
            "Authorization": f"Bearer {token}",
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    ) as session:
        post = await session.post(
            f"{str(gateway).rstrip('/')}/api/v1/message", data=payload.model_dump_json()
        )
        await verify_response_status(post, status=(200, 201, 204))
        if post.status == 201:
            return NewProcessResponseWithVariables(**await post.json())
        else:
            return CorrelatedProcessResponseWithVariables(**await post.json())


@task(topic=settings.TASKS_TOPIC)  # type: ignore
async def handler(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Update health check timestamp."""
    # Resolve message name
    xml = await get_xml(task.processDefinitionId)
    messageName = (
        task.variables.pop("messageName").value
        if "messageName" in task.variables
        else None
    )
    for event in xml.xpath(
        f"//*[@id='{task.activityId}']/bpmn:messageEventDefinition", namespaces=NS
    ):
        for message in xml.xpath(
            f"//bpmn:message[@id='{event.get('messageRef')}']", namespaces=NS
        ):
            messageName = message.get("name")
    assert messageName, "messageName not found in task variables or BPMN XML"

    # Resolve gateway configuration
    assert (
        messageName in settings.TASKS
    ), f"Gateway configuration missing for message {messageName}"
    config = settings.TASKS[messageName]

    # Validate against schema
    schema = json.loads(config.json_schema)
    if "outputParameters" in schema["properties"]:
        del schema["properties"]["outputParameters"]
        schema["required"] = [
            key for key in schema["required"] if key != "outputParameters"
        ]
    jsonschema.validate(
        {"messageName": messageName, "inputParameters": load_variables(task.variables)},
        schema,
    )

    def typed_value(
        value: Any, type: str, valueInfo: Optional[Dict[str, Any]] = None
    ) -> TypedValue:
        """Create a TypedValue from a value and type."""
        # Engine dates are strings in JSON schema
        if isinstance(value, str) and RE_ENGINE_DATE.match(value):
            return TypedValue(value=value, type="Date")
        return TypedValue(value=value, type=type)

    # Dispatch message
    response = await correlate(
        config.url,
        config.token,
        MessageParams(
            messageName=messageName,
            businessKey=task.businessKey,
            processVariables={
                key: typed_value(**value.model_dump())
                for key, value in task.variables.items()
            },
            variablesInResultEnabled=True,
        ),
    )

    # Build response
    variables = (
        {"businessKey": VariableValueDto(value=response.businessKey, type="String")}
        if isinstance(response, NewProcessResponseWithVariables)
        else (
            {}
            | {
                key: VariableValueDto(**value.model_dump())
                for key, value in (response.variables or {}).items()
            }
            if isinstance(response, NewProcessResponseWithVariables)
            or isinstance(response, CorrelatedProcessResponseWithVariables)
            else {}
        )
    )

    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(
            workerId=task.workerId,
            variables=variables,
        ),
    )
