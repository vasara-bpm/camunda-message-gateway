"""Heartbeat type definitions."""

from pydantic import BaseModel
from pydantic import Field


class Heartbeat(BaseModel):
    """Health check response."""

    timestamp: str = Field(
        "",
        description="UTC timestamp of the last recorded heartbeat.",
    )
