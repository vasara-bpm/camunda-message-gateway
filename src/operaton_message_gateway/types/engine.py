"""Types for internal requests to BPM engine."""

from pydantic import BaseModel
from typing import Any
from typing import Dict
from typing import Optional
import operaton_message_gateway.config as cfg


class TypedValue(BaseModel):
    """Typed variables in Operaton."""

    value: Any
    type: str

    def to_dict(self) -> Dict[str, Any]:
        """Transform to dict."""
        return {"value": self.value, "type": self.type}


class CorrelationKeyMap(BaseModel):
    """Inner json for the localCorrelationKeys field of MessageToProcess."""

    messageCorrelationId: Optional[TypedValue]

    def to_dict(self) -> Dict[str, Any]:
        """Transform to dict."""
        return (
            {cfg.settings.MESSAGE_CORRELATION_NAME: self.messageCorrelationId.__dict__}
            if self.messageCorrelationId
            else {}
        )


class MessageToProcess(BaseModel):
    """Request to send a message to an existing process."""

    messageName: str
    businessKey: Optional[str]
    localCorrelationKeys: CorrelationKeyMap
    processVariablesLocal: Dict[str, TypedValue]
    resultEnabled: bool = True
    variablesInResultEnabled: bool = False
    all: bool = False

    def to_dict(self) -> Dict[str, Any]:
        """Transform to dict."""
        return {
            **self.__dict__,
            "processVariablesLocal": {
                k: v.to_dict() for k, v in self.processVariablesLocal.items()
            },
            "localCorrelationKeys": self.localCorrelationKeys.to_dict(),
        }
