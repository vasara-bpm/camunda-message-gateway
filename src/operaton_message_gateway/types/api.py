"""API type definitions."""

from datetime import datetime
from datetime import timezone
from operaton_message_gateway.types.engine import TypedValue
from pydantic import BaseModel
from pydantic import model_validator
from typing import Any
from typing import Dict
from typing import Optional
from typing import Union
from typing_extensions import Self


class MessagingToken(BaseModel):
    """Token identifying the sender of a request."""

    name: str  # BPMN message name prefix for the token
    exp: Optional[int] = None  # ISO 8601 timestamp when the token expires
    expires_in: Optional[int] = None

    @model_validator(mode="after")
    def validate_expires_in(self) -> Self:
        """Calculate expires_in from exp if exp is present."""
        if self.exp:
            self.expires_in = int(self.exp - datetime.now(tz=timezone.utc).timestamp())
        return self


class MessageParams(BaseModel):
    """Parameters to the /api/v1/message endpoint."""

    messageName: Optional[str] = None
    businessKey: Optional[str] = None
    messageCorrelationId: Optional[str] = None
    processVariables: Optional[Dict[str, TypedValue]] = None
    variablesInResultEnabled: bool = False


class NewProcessResponse(BaseModel):
    """Response from /api/v1/message when a new process was created."""

    businessKey: str


class NewProcessResponseWithVariables(NewProcessResponse):
    """Response from /api/v1/message when a new process was created."""

    variables: Optional[Dict[str, TypedValue]] = None


class CorrelatedProcessResponse(BaseModel):
    """Response from /api/v1/message when an existing process was correlated."""


class CorrelatedProcessResponseWithVariables(CorrelatedProcessResponse):
    """Response from /api/v1/message when an existing process was correlated."""

    variables: Optional[Dict[str, TypedValue]] = None


# A plain Response with no body and status code 204 is returned
# when a message is sent to an existing process and no variables,
# are requested for return. Otherwise NewProcessResponse with status 201.
MessageResponse = Union[
    # When defining a Union, include the most specific type first,
    # followed by the less specific type. ORDER DOES MATTER!
    NewProcessResponseWithVariables,
    CorrelatedProcessResponseWithVariables,
    NewProcessResponse,
    CorrelatedProcessResponse,
]


class MessageError(BaseModel):
    """Simplified error returned from /api/v1/message."""

    message: str
    # also return original Operaton error in case we misinterpret it
    originalError: Optional[Dict[str, Any]] = None
